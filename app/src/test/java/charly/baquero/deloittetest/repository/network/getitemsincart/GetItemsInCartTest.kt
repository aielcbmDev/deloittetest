package charly.baquero.deloittetest.repository.network.getitemsincart

import charly.baquero.deloittetest.repository.network.getitemsincart.response.ItemInCart
import charly.baquero.deloittetest.utils.RetrySettings
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

private const val SUCCESSFUL_GET_ITEMS_IN_CART_DATA =
    "[{\"id\":1,\"productId\":1},{\"id\":2,\"productId\":1},{\"id\":3,\"productId\":3},{\"id\":4,\"productId\":3}]"

class GetItemsInCartTest {

    private val productsListType: Type =
        Types.newParameterizedType(MutableList::class.java, ItemInCart::class.java)
    private val jsonAdapter: JsonAdapter<List<ItemInCart>> =
        Moshi.Builder().build().adapter(productsListType)

    private lateinit var getItemsInCartService: GetItemsInCartService
    private lateinit var retrySettings: RetrySettings
    private lateinit var getItemsInCartDataSource: GetItemsInCartDataSource

    @Before
    fun setUp() {
        getItemsInCartService = mock()
        retrySettings = mock()
        getItemsInCartDataSource = GetItemsInCart(getItemsInCartService, retrySettings)
    }

    @Test
    fun test_successful_getItemsInCartMap() {
        // Given
        whenever(getItemsInCartService.getItemsInCartList())
            .thenReturn(Single.just(jsonAdapter.fromJson(SUCCESSFUL_GET_ITEMS_IN_CART_DATA)))

        whenever(retrySettings.getNumberOfRetries()).thenReturn(3)
        whenever(retrySettings.getRetriesTimeUnit()).thenReturn(TimeUnit.MILLISECONDS)
        whenever(retrySettings.getTimeBetweenRetries()).thenReturn(500L)

        // When
        val productIdToCardIdListMap = getItemsInCartDataSource.getItemsInCartMap().blockingGet()

        // Then
        val cartIdListForProductItem1 = productIdToCardIdListMap[1]
        assertNotNull(cartIdListForProductItem1)
        assertEquals(2, cartIdListForProductItem1?.size)
        assertEquals(1L, cartIdListForProductItem1?.get(0))
        assertEquals(2L, cartIdListForProductItem1?.get(1))

        val cartIdListForProductItem3 = productIdToCardIdListMap[3]
        assertNotNull(cartIdListForProductItem3)
        assertEquals(2, cartIdListForProductItem3?.size)
        assertEquals(3L, cartIdListForProductItem3?.get(0))
        assertEquals(4L, cartIdListForProductItem3?.get(1))
    }
}