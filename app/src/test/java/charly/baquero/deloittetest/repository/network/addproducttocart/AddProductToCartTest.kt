package charly.baquero.deloittetest.repository.network.addproducttocart

import charly.baquero.deloittetest.utils.RetrySettings
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

class AddProductToCartTest {

    private lateinit var addProductToCartService: AddProductToCartService
    private lateinit var retrySettings: RetrySettings
    private lateinit var addProductToCartDataSource: AddProductToCartDataSource

    @Before
    fun setUp() {
        addProductToCartService = mock()
        retrySettings = mock()
        addProductToCartDataSource = AddProductToCart(addProductToCartService, retrySettings)
    }

    @Test
    fun test_successful_AddProductToCart() {
        // Given
        val productId = 1L
        whenever(addProductToCartService.addProductToCart(productId))
            .thenReturn(Completable.complete())

        whenever(retrySettings.getNumberOfRetries()).thenReturn(3)
        whenever(retrySettings.getRetriesTimeUnit()).thenReturn(TimeUnit.MILLISECONDS)
        whenever(retrySettings.getTimeBetweenRetries()).thenReturn(500L)

        // When
        val testObserver = addProductToCartDataSource.addProductToCart(productId).test()

        // Then
        testObserver.assertNoErrors()
    }
}