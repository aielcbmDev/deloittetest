package charly.baquero.deloittetest.repository.network.getproducts

import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import charly.baquero.deloittetest.utils.RetrySettings
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

private const val SUCCESSFUL_GET_PRODUCTS_LIST_DATA =
    "[{\"id\":1,\"name\":\"Almond Toe Court Shoes, Patent Black\",\"category\":\"Women’s Footwear\",\"price\":\"99.00\",\"oldPrice\":null,\"stock\":5},{\"id\":2,\"name\":\"Suede Shoes, Blue\",\"category\":\"Women’s Footwear\",\"price\":\"42.00\",\"oldPrice\":null,\"stock\":4},{\"id\":3,\"name\":\"Leather Driver Saddle Loafers, Tan\",\"category\":\"Men’s Footwear\",\"price\":\"34.00\",\"oldPrice\":null,\"stock\":12},{\"id\":4,\"name\":\"Flip Flops, Red\",\"category\":\"Men’s Footwear\",\"price\":\"19.00\",\"oldPrice\":null,\"stock\":6},{\"id\":5,\"name\":\"Flip Flops, Blue\",\"category\":\"Men’s Footwear\",\"price\":\"19.00\",\"oldPrice\":null,\"stock\":0},{\"id\":6,\"name\":\"Gold Button Cardigan, Black\",\"category\":\"Women’s Casualwear\",\"price\":\"167.00\",\"oldPrice\":null,\"stock\":6},{\"id\":7,\"name\":\"Cotton Shorts, Medium Red\",\"category\":\"Women’s Casualwear\",\"price\":\"30.00\",\"oldPrice\":null,\"stock\":5},{\"id\":8,\"name\":\"Fine Stripe Short Sleeve Shirt, Grey\",\"category\":\"Men’s Casualwear\",\"price\":\"49.99\",\"oldPrice\":null,\"stock\":9},{\"id\":9,\"name\":\"Fine Stripe Short Sleeve Shirt, Green\",\"category\":\"Men’s Casualwear\",\"price\":\"39.99\",\"oldPrice\":\"49.99\",\"stock\":3},{\"id\":10,\"name\":\"Sharkskin Waistcoat, Charcoal\",\"category\":\"Men’s Formalwear\",\"price\":\"75.00\",\"oldPrice\":null,\"stock\":2},{\"id\":11,\"name\":\"Lightweight Patch Pocket Blazer, Deer\",\"category\":\"Men’s Formalwear\",\"price\":\"175.00\",\"oldPrice\":null,\"stock\":1},{\"id\":12,\"name\":\"Bird Print Dress, Black\",\"category\":\"Women’s Formalwear\",\"price\":\"270.00\",\"oldPrice\":null,\"stock\":10},{\"id\":13,\"name\":\"Mid Twist Cut-Out Dress, Pink\",\"category\":\"Women’s Formalwear\",\"price\":\"540.00\",\"oldPrice\":null,\"stock\":5}]"

class GetProductsListTest {

    private val productsListType: Type =
        Types.newParameterizedType(MutableList::class.java, Product::class.java)
    private val jsonAdapter: JsonAdapter<List<Product>> =
        Moshi.Builder().build().adapter(productsListType)

    private lateinit var getProductsListService: GetProductsListService
    private lateinit var retrySettings: RetrySettings
    private lateinit var getProductsListDataSource: GetProductsListDataSource

    @Before
    fun setUp() {
        getProductsListService = mock()
        retrySettings = mock()
        getProductsListDataSource = GetProductsList(getProductsListService, retrySettings)
    }

    @Test
    fun test_successful_getProductsList() {
        // Given
        whenever(getProductsListService.getProductsList())
            .thenReturn(Single.just(jsonAdapter.fromJson(SUCCESSFUL_GET_PRODUCTS_LIST_DATA)))

        whenever(retrySettings.getNumberOfRetries()).thenReturn(3)
        whenever(retrySettings.getRetriesTimeUnit()).thenReturn(TimeUnit.MILLISECONDS)
        whenever(retrySettings.getTimeBetweenRetries()).thenReturn(500L)

        // When
        val productsList = getProductsListDataSource.getProductsList().blockingGet()

        // Then
        assertNotNull(productsList)
        assertEquals(13, productsList.size)

        val product0 = productsList[0]
        assertEquals(1L, product0.id)
        assertEquals("Almond Toe Court Shoes, Patent Black", product0.name)
        assertEquals("99.00", product0.price)
        assertNull(product0.oldPrice)
        assertEquals(5, product0.stock)
        assertEquals("Women’s Footwear", product0.category)

        val product1 = productsList[1]
        assertEquals(2L, product1.id)
        assertEquals("Suede Shoes, Blue", product1.name)
        assertEquals("42.00", product1.price)
        assertNull(product1.oldPrice)
        assertEquals(4, product1.stock)
        assertEquals("Women’s Footwear", product1.category)

        val product2 = productsList[2]
        assertEquals(3L, product2.id)
        assertEquals("Leather Driver Saddle Loafers, Tan", product2.name)
        assertEquals("34.00", product2.price)
        assertNull(product2.oldPrice)
        assertEquals(12, product2.stock)
        assertEquals("Men’s Footwear", product2.category)

        val product3 = productsList[3]
        assertEquals(4L, product3.id)
        assertEquals("Flip Flops, Red", product3.name)
        assertEquals("19.00", product3.price)
        assertNull(product3.oldPrice)
        assertEquals(6, product3.stock)
        assertEquals("Men’s Footwear", product3.category)

        val product4 = productsList[4]
        assertEquals(5L, product4.id)
        assertEquals("Flip Flops, Blue", product4.name)
        assertEquals("19.00", product4.price)
        assertNull(product4.oldPrice)
        assertEquals(0, product4.stock)
        assertEquals("Men’s Footwear", product4.category)

        val product5 = productsList[5]
        assertEquals(6L, product5.id)
        assertEquals("Gold Button Cardigan, Black", product5.name)
        assertEquals("167.00", product5.price)
        assertNull(product5.oldPrice)
        assertEquals(6, product5.stock)
        assertEquals("Women’s Casualwear", product5.category)

        val product6 = productsList[6]
        assertEquals(7L, product6.id)
        assertEquals("Cotton Shorts, Medium Red", product6.name)
        assertEquals("30.00", product6.price)
        assertNull(product6.oldPrice)
        assertEquals(5, product6.stock)
        assertEquals("Women’s Casualwear", product6.category)

        val product7 = productsList[7]
        assertEquals(8L, product7.id)
        assertEquals("Fine Stripe Short Sleeve Shirt, Grey", product7.name)
        assertEquals("49.99", product7.price)
        assertNull(product7.oldPrice)
        assertEquals(9, product7.stock)
        assertEquals("Men’s Casualwear", product7.category)

        val product8 = productsList[8]
        assertEquals(9L, product8.id)
        assertEquals("Fine Stripe Short Sleeve Shirt, Green", product8.name)
        assertEquals("39.99", product8.price)
        assertEquals("49.99", product8.oldPrice)
        assertEquals(3, product8.stock)
        assertEquals("Men’s Casualwear", product8.category)

        val product9 = productsList[9]
        assertEquals(10L, product9.id)
        assertEquals("Sharkskin Waistcoat, Charcoal", product9.name)
        assertEquals("75.00", product9.price)
        assertNull(product9.oldPrice)
        assertEquals(2, product9.stock)
        assertEquals("Men’s Formalwear", product9.category)

        val product10 = productsList[10]
        assertEquals(11L, product10.id)
        assertEquals("Lightweight Patch Pocket Blazer, Deer", product10.name)
        assertEquals("175.00", product10.price)
        assertNull(product10.oldPrice)
        assertEquals(1, product10.stock)
        assertEquals("Men’s Formalwear", product10.category)

        val product11 = productsList[11]
        assertEquals(12L, product11.id)
        assertEquals("Bird Print Dress, Black", product11.name)
        assertEquals("270.00", product11.price)
        assertNull(product11.oldPrice)
        assertEquals(10, product11.stock)
        assertEquals("Women’s Formalwear", product11.category)

        val product12 = productsList[12]
        assertEquals(13L, product12.id)
        assertEquals("Mid Twist Cut-Out Dress, Pink", product12.name)
        assertEquals("540.00", product12.price)
        assertNull(product12.oldPrice)
        assertEquals(5, product12.stock)
        assertEquals("Women’s Formalwear", product12.category)
    }
}