package charly.baquero.deloittetest.utils

import charly.baquero.RxImmediateSchedulerRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.functions.Consumer
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import java.util.concurrent.TimeUnit

class RetryFunctionTest {

    private lateinit var getDummyDataDataSource: GetDummyDataDataSource

    @Before
    fun setUp() {
        getDummyDataDataSource = mock()
    }

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    @Test
    fun test_Succeed() {
        // Given
        val numberOfRetries = 3
        var retriesCount = 0

        whenever(getDummyDataDataSource.getData()).thenReturn(
            Single.just("Some data")
        )

        // When
        val data =
            getDummyDataDataSource.getData()
                .doOnError(Consumer { retriesCount++ })
                .retryWhen(RetryFunction(numberOfRetries, 8L, TimeUnit.SECONDS))
                .blockingGet()

        // Then
        assertEquals(0, retriesCount)
        assertEquals("Some data", data)
    }

    @Test
    fun test_50_Retries_And_Fail() {
        // Given
        val errorMessage = "getData terminated with an error"
        val numberOfRetries = 50
        var retriesCount = 0

        whenever(getDummyDataDataSource.getData())
            .thenReturn(
                Single.error(Throwable("Dummy Throwable"))
            )

        // When
        val data =
            getDummyDataDataSource.getData()
                .doOnError(Consumer { retriesCount++ })
                .retryWhen(RetryFunction(numberOfRetries, 8L, TimeUnit.SECONDS))
                .onErrorReturn { errorMessage }
                .blockingGet()

        // Then
        assertEquals(numberOfRetries + 1, retriesCount)
        assertEquals(errorMessage, data)
    }

    interface GetDummyDataDataSource {

        fun getData(): Single<String>
    }
}