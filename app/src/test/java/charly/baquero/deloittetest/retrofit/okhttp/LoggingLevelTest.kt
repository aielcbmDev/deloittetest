package charly.baquero.deloittetest.retrofit.okhttp

import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Assert
import org.junit.Test

class LoggingLevelTest {

    @Test
    fun test_LoggingLevelBody() {
        // Given
        val isDebugBuild = true

        // When
        val loggingLevel = LoggingLevel(isDebugBuild)

        // Then
        val httpLoggingInterceptor = loggingLevel.provideHttpLoggingInterceptor()
        Assert.assertEquals(HttpLoggingInterceptor.Level.BODY, httpLoggingInterceptor.level)
    }

    @Test
    fun test_LoggingLevelNone() {
        // Given
        val isDebugBuild = false

        // When
        val loggingLevel = LoggingLevel(isDebugBuild)

        // Then
        val httpLoggingInterceptor = loggingLevel.provideHttpLoggingInterceptor()
        Assert.assertEquals(HttpLoggingInterceptor.Level.NONE, httpLoggingInterceptor.level)
    }
}