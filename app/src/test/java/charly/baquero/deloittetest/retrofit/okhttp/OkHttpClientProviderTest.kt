package charly.baquero.deloittetest.retrofit.okhttp

import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.TimeUnit

class OkHttpClientProviderTest {

    @Test
    fun test_InputValues_1() {
        // Given
        val loggingLevel = LoggingLevel(true)
        val timeout = 50L
        val timeUnit = TimeUnit.SECONDS

        // When
        val okHttpClientProvider =
            OkHttpClientProvider(
                loggingLevel,
                timeout,
                timeUnit
            )

        // Then
        Assert.assertTrue(okHttpClientProvider.sharedOkHttpClient.interceptors[0] is HttpLoggingInterceptor)
        val httpLoggingInterceptor =
            okHttpClientProvider.sharedOkHttpClient.interceptors[0] as HttpLoggingInterceptor
        Assert.assertEquals(HttpLoggingInterceptor.Level.BODY, httpLoggingInterceptor.level)
        Assert.assertEquals(
            timeUnit.toMillis(timeout).toInt(),
            okHttpClientProvider.sharedOkHttpClient.connectTimeoutMillis
        )
    }

    @Test
    fun test_InputValues_2() {
        // Given
        val loggingLevel = LoggingLevel(false)
        val timeout = 780L
        val timeUnit = TimeUnit.MINUTES

        // When
        val okHttpClientProvider =
            OkHttpClientProvider(
                loggingLevel,
                timeout,
                timeUnit
            )

        // Then
        Assert.assertTrue(okHttpClientProvider.sharedOkHttpClient.interceptors[0] is HttpLoggingInterceptor)
        val httpLoggingInterceptor =
            okHttpClientProvider.sharedOkHttpClient.interceptors[0] as HttpLoggingInterceptor
        Assert.assertEquals(HttpLoggingInterceptor.Level.NONE, httpLoggingInterceptor.level)
        Assert.assertEquals(
            timeUnit.toMillis(timeout).toInt(),
            okHttpClientProvider.sharedOkHttpClient.connectTimeoutMillis
        )
    }
}