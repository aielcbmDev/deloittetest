package charly.baquero.deloittetest.ui.productslist.views.productlistitem

import android.view.View
import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import com.nhaarman.mockitokotlin2.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test

class ProductListItemPresenterTest {

    private lateinit var productListItemView: ProductListItemViewContract.View
    private lateinit var productListItemPresenter: ProductListItemViewContract.Presenter

    @Before
    fun setUp() {
        productListItemView = mock()
        productListItemPresenter = ProductListItemPresenter(productListItemView)
    }

    @Test
    fun test_setProduct_thatIsNotDiscounted_withItemsInStock() {
        // Given
        val product = Product(
            1L,
            "Almond Toe Court Shoes, Patent Black",
            "Women’s Footwear",
            "99.00",
            null,
            5
        )

        // When
        productListItemPresenter.setProduct(product)

        // Then
        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setName(capture())
            assertEquals(product.name, firstValue)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setCategory(capture())
            assertEquals(product.category, firstValue)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setPrice(capture())
            assertEquals(product.price, firstValue)
        }

        argumentCaptor<Int>().apply {
            verify(productListItemView, times(1)).setOldPriceVisibility(capture())
            assertEquals(View.INVISIBLE, firstValue)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, never()).setOldPrice(capture())
            assertNull(product.oldPrice)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setItemsInStock(capture())
            assertEquals(product.stock.toString(), firstValue)
        }

        argumentCaptor<Int>().apply {
            verify(productListItemView, times(1)).setAddButtonVisibility(capture())
            assertEquals(View.VISIBLE, firstValue)
        }

        argumentCaptor<Product>().apply {
            verify(productListItemView, times(1)).setAddButtonTag(capture())
            assertEquals(product, firstValue)
        }
    }

    @Test
    fun test_setProduct_thatIsDiscounted_withItemsInStock() {
        // Given
        val product = Product(
            9L,
            "Fine Stripe Short Sleeve Shirt, Green",
            "Men’s Casualwear",
            "39.99",
            "49.99",
            3
        )

        // When
        productListItemPresenter.setProduct(product)

        // Then
        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setName(capture())
            assertEquals(product.name, firstValue)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setCategory(capture())
            assertEquals(product.category, firstValue)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setPrice(capture())
            assertEquals(product.price, firstValue)
        }

        argumentCaptor<Int>().apply {
            verify(productListItemView, times(1)).setOldPriceVisibility(capture())
            assertEquals(View.VISIBLE, firstValue)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setOldPrice(capture())
            assertEquals(product.oldPrice, firstValue)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setItemsInStock(capture())
            assertEquals(product.stock.toString(), firstValue)
        }

        argumentCaptor<Int>().apply {
            verify(productListItemView, times(1)).setAddButtonVisibility(capture())
            assertEquals(View.VISIBLE, firstValue)
        }

        argumentCaptor<Product>().apply {
            verify(productListItemView, times(1)).setAddButtonTag(capture())
            assertEquals(product, firstValue)
        }
    }

    @Test
    fun test_setProduct_thatIsNotDiscounted_withNoItemsInStock() {
        // Given
        val product = Product(
            5L,
            "Flip Flops, Blue",
            "Men’s Footwear",
            "19.00",
            null,
            0
        )

        // When
        productListItemPresenter.setProduct(product)

        // Then
        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setName(capture())
            assertEquals(product.name, firstValue)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setCategory(capture())
            assertEquals(product.category, firstValue)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setPrice(capture())
            assertEquals(product.price, firstValue)
        }

        argumentCaptor<Int>().apply {
            verify(productListItemView, times(1)).setOldPriceVisibility(capture())
            assertEquals(View.INVISIBLE, firstValue)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, never()).setOldPrice(capture())
            assertNull(product.oldPrice)
        }

        argumentCaptor<String>().apply {
            verify(productListItemView, times(1)).setItemsInStock(capture())
            assertEquals(product.stock.toString(), firstValue)
        }

        argumentCaptor<Int>().apply {
            verify(productListItemView, times(1)).setAddButtonVisibility(capture())
            assertEquals(View.INVISIBLE, firstValue)
        }

        argumentCaptor<Product>().apply {
            verify(productListItemView, never()).setAddButtonTag(capture())
        }
    }
}