package charly.baquero.deloittetest.repository.network.getitemsincart.response

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ItemInCart(
    @Json(name = "id") val cartId: Long,
    @Json(name = "productId") val productId: Long
) : Parcelable