package charly.baquero.deloittetest.repository.network.getitemsincart.response

import android.os.Parcelable
import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class ProductInCart(
    @Json(name = "id") val id: Long,
    @Json(name = "product") val product: Product
) : Parcelable {

    fun getProductId(): Long {
        return product.id
    }

    fun getProductName(): String {
        return product.name
    }

    fun getProductCategory(): String {
        return product.category
    }

    fun getProductPrice(): String {
        return product.price
    }

    fun getProductOldPrice(): String? {
        return product.oldPrice
    }

    fun getProductStock(): Int {
        return product.stock
    }

    fun isDiscounted(): Boolean {
        return product.isDiscounted()
    }
}