package charly.baquero.deloittetest.repository.network.getproducts

import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import io.reactivex.Single

interface GetProductsListDataSource {

    fun getProductsList(): Single<List<Product>>
}