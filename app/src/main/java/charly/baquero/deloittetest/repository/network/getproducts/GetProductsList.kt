package charly.baquero.deloittetest.repository.network.getproducts

import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import charly.baquero.deloittetest.utils.RetryFunction
import charly.baquero.deloittetest.utils.RetrySettings
import io.reactivex.Single

class GetProductsList(
    private val getProductsListService: GetProductsListService,
    private val retrySettings: RetrySettings
) : GetProductsListDataSource {

    override fun getProductsList(): Single<List<Product>> {
        return getProductsListService.getProductsList()
            .retryWhen(
                RetryFunction(
                    retrySettings.getNumberOfRetries(),
                    retrySettings.getTimeBetweenRetries(),
                    retrySettings.getRetriesTimeUnit()
                )
            )
    }
}