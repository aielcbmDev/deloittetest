package charly.baquero.deloittetest.repository.network.addproducttocart

import io.reactivex.Completable

interface AddProductToCartDataSource {

    fun addProductToCart(productId: Long): Completable
}