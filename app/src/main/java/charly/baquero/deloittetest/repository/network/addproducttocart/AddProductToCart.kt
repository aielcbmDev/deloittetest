package charly.baquero.deloittetest.repository.network.addproducttocart

import charly.baquero.deloittetest.utils.RetryFunction
import charly.baquero.deloittetest.utils.RetrySettings
import io.reactivex.Completable

class AddProductToCart(
    private val addProductToCartService: AddProductToCartService,
    private val retrySettings: RetrySettings
) : AddProductToCartDataSource {

    override fun addProductToCart(productId: Long): Completable {
        return addProductToCartService.addProductToCart(productId)
            .retryWhen(
                RetryFunction(
                    retrySettings.getNumberOfRetries(),
                    retrySettings.getTimeBetweenRetries(),
                    retrySettings.getRetriesTimeUnit()
                )
            )
    }
}