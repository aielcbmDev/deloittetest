package charly.baquero.deloittetest.repository.network.getitemsincart

import io.reactivex.Single

interface GetItemsInCartDataSource {

    fun getItemsInCartMap(): Single<Map<Long, MutableList<Long>>>
}