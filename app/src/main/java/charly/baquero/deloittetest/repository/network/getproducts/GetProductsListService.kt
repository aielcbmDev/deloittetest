package charly.baquero.deloittetest.repository.network.getproducts

import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers

interface GetProductsListService {

    @Headers("Content-Type: application/json")
    @GET("products")
    fun getProductsList(): Single<List<Product>>
}