package charly.baquero.deloittetest.repository

import charly.baquero.deloittetest.repository.network.addproducttocart.AddProductToCartDataSource
import charly.baquero.deloittetest.repository.network.getitemsincart.GetItemsInCartDataSource
import charly.baquero.deloittetest.repository.network.getitemsincart.response.ProductInCart
import charly.baquero.deloittetest.repository.network.getproducts.GetProductsListDataSource
import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function

class ClothesDataRepository(
    private val getProductsListDataSource: GetProductsListDataSource,
    private val addProductToCartDataSource: AddProductToCartDataSource,
    private val getItemsInCartDataSource: GetItemsInCartDataSource
) : ClothesRepository {

    override fun getProductsList(): Single<List<Product>> {
        return getProductsListDataSource.getProductsList()
    }

    override fun addProductToCart(productId: Long): Single<List<Product>> {
        return addProductToCartDataSource.addProductToCart(productId)
            .andThen(Observable.just(true))
            .flatMap(Function<Boolean, ObservableSource<List<Product>>> {
                getProductsListDataSource.getProductsList().toObservable()
            } as Function<Boolean, ObservableSource<List<Product>>>?,
                BiFunction<Boolean, List<Product>, List<Product>> { _, productList -> productList })
            .singleOrError()
    }

    override fun getItemsInCartList(): Single<List<ProductInCart>> {
        return getItemsInCartDataSource.getItemsInCartMap()
            .toObservable()
            .flatMap(Function<Map<Long, MutableList<Long>>, ObservableSource<List<Product>>> {
                getProductsListDataSource.getProductsList().toObservable()
            } as Function<Map<Long, MutableList<Long>>, ObservableSource<List<Product>>>?,
                BiFunction<Map<Long, MutableList<Long>>, List<Product>, List<ProductInCart>> { itemsInCartMap, productList ->
                    mergeItemsInCartWithProductsList(itemsInCartMap, productList)
                })
            .singleOrError()
    }

    private fun mergeItemsInCartWithProductsList(
        itemsInCartMap: Map<Long, MutableList<Long>>,
        productList: List<Product>
    ): List<ProductInCart> {
        val productsInCartList = mutableListOf<ProductInCart>()
        for (product in productList) {
            val cartIdList = itemsInCartMap[product.id]
            if (!cartIdList.isNullOrEmpty()) {
                for (cartId in cartIdList) {
                    productsInCartList.add(ProductInCart(cartId, product))
                }
            }
        }
        return productsInCartList
    }
}