package charly.baquero.deloittetest.repository.network.addproducttocart

import charly.baquero.deloittetest.utils.RetrySettings
import java.util.concurrent.TimeUnit

class AddProductToCartRetrySettings(
    private val numberOfRetries: Int,
    private val timeBetweenRetries: Long,
    private var retriesTimeUnit: TimeUnit
) : RetrySettings {

    override fun getNumberOfRetries(): Int {
        return numberOfRetries
    }

    override fun getTimeBetweenRetries(): Long {
        return timeBetweenRetries
    }

    override fun getRetriesTimeUnit(): TimeUnit {
        return retriesTimeUnit
    }
}