package charly.baquero.deloittetest.repository

import charly.baquero.deloittetest.repository.network.getitemsincart.response.ProductInCart
import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import io.reactivex.Single

interface ClothesRepository {

    fun getProductsList(): Single<List<Product>>

    fun addProductToCart(productId: Long): Single<List<Product>>

    fun getItemsInCartList(): Single<List<ProductInCart>>
}