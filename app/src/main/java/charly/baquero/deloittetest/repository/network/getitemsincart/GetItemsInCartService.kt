package charly.baquero.deloittetest.repository.network.getitemsincart

import charly.baquero.deloittetest.repository.network.getitemsincart.response.ItemInCart
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers

interface GetItemsInCartService {

    @Headers("Content-Type: application/json")
    @GET("cart")
    fun getItemsInCartList(): Single<List<ItemInCart>>
}