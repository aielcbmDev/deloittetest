package charly.baquero.deloittetest.repository.network.addproducttocart

import io.reactivex.Completable
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

interface AddProductToCartService {

    @Headers("Content-Type: application/json")
    @POST("cart")
    fun addProductToCart(@Query("productId") productId: Long): Completable
}