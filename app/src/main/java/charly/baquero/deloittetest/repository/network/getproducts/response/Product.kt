package charly.baquero.deloittetest.repository.network.getproducts.response

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Product(
    @Json(name = "id") val id: Long,
    @Json(name = "name") val name: String,
    @Json(name = "category") val category: String,
    @Json(name = "price") val price: String,
    @Json(name = "oldPrice") val oldPrice: String?,
    @Json(name = "stock") val stock: Int
) : Parcelable {

    fun isDiscounted(): Boolean {
        return !oldPrice.isNullOrEmpty()
    }

    fun isInStock(): Boolean {
        return stock > 0
    }
}