package charly.baquero.deloittetest.repository.network.getitemsincart

import charly.baquero.deloittetest.utils.RetryFunction
import charly.baquero.deloittetest.utils.RetrySettings
import io.reactivex.Single

class GetItemsInCart(
    private val getItemsInCartService: GetItemsInCartService,
    private val retrySettings: RetrySettings
) : GetItemsInCartDataSource {

    override fun getItemsInCartMap(): Single<Map<Long, MutableList<Long>>> {
        return getItemsInCartService.getItemsInCartList()
            .retryWhen(
                RetryFunction(
                    retrySettings.getNumberOfRetries(),
                    retrySettings.getTimeBetweenRetries(),
                    retrySettings.getRetriesTimeUnit()
                )
            )
            .map { list ->
                val productIdToCardIdListMap = HashMap<Long, MutableList<Long>>()
                for (itemInCart in list) {
                    var cartIdList: MutableList<Long>? =
                        productIdToCardIdListMap[itemInCart.productId]
                    if (cartIdList == null) {
                        cartIdList = mutableListOf()
                    }
                    cartIdList.add(itemInCart.cartId)
                    productIdToCardIdListMap[itemInCart.productId] = cartIdList
                }
                productIdToCardIdListMap
            }
    }
}