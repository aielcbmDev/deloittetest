package charly.baquero.deloittetest.di

import charly.baquero.deloittetest.di.scopes.PerActivity
import charly.baquero.deloittetest.ui.MainActivity
import charly.baquero.deloittetest.ui.MainActivityModule
import charly.baquero.deloittetest.ui.productslist.ProductsListFragmentDependenciesProvider
import charly.baquero.deloittetest.ui.shoppingcart.ShoppingCartFragmentDependenciesProvider
import charly.baquero.deloittetest.ui.wishlist.WishListFragmentDependenciesProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(
        modules = [
            MainActivityModule::class,
            ProductsListFragmentDependenciesProvider::class,
            ShoppingCartFragmentDependenciesProvider::class,
            WishListFragmentDependenciesProvider::class
        ]
    )
    @PerActivity
    abstract fun bindMainActivity(): MainActivity
}