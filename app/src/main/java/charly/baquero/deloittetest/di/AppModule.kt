package charly.baquero.deloittetest.di

import android.app.Application
import charly.baquero.deloittetest.BuildConfig
import charly.baquero.deloittetest.DeloitteApplication
import charly.baquero.deloittetest.retrofit.okhttp.AddApiKeyInterceptor
import charly.baquero.deloittetest.retrofit.okhttp.LoggingLevel
import charly.baquero.deloittetest.retrofit.okhttp.OkHttpClientProvider
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object AppModule {

    @Provides
    @JvmStatic
    @Singleton
    fun provideDeloitteApplication(application: Application): DeloitteApplication {
        return application as DeloitteApplication
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideSharedOkHttpClient(): OkHttpClient {
        val addApiKeyInterceptor = AddApiKeyInterceptor(BuildConfig.API_KEY)
        val loggingLevel = LoggingLevel(BuildConfig.DEBUG)
        val okHttpClientProvider =
            OkHttpClientProvider(loggingLevel, 500L, TimeUnit.MILLISECONDS, addApiKeyInterceptor)
        return okHttpClientProvider.sharedOkHttpClient
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideMoshi(): Moshi {
        return Moshi.Builder().build()
    }

    @Provides
    @JvmStatic
    @Singleton
    fun provideBaseUrl(): String {
        return BuildConfig.BASE_URL
    }
}