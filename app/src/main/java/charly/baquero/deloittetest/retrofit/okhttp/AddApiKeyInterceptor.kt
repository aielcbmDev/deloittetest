package charly.baquero.deloittetest.retrofit.okhttp

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

private const val API_KEY_HEADER_NAME = "X-API-KEY"

class AddApiKeyInterceptor(private val apiKey: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val newRequest = processRequest(originalRequest)
        return chain.proceed(newRequest)
    }

    private fun processRequest(originalRequest: Request): Request {
        val builder = originalRequest.newBuilder()
        builder.addHeader(API_KEY_HEADER_NAME, apiKey)
        return builder.build()
    }
}