package charly.baquero.deloittetest.retrofit.okhttp

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class OkHttpClientProvider(
    loggingLevel: LoggingLevel,
    timeout: Long,
    timeUnit: TimeUnit,
    vararg interceptorArray: Interceptor
) {

    /**
     * OkHttp performs best when you create a single `OkHttpClient` instance and reuse it for all of
     * your HTTP calls. This is because each client holds its own connection pool and thread pools.
     * Reusing connections and threads reduces latency and saves memory. Conversely, creating a client
     * for each request wastes resources on idle pools.
     */
    val sharedOkHttpClient: OkHttpClient

    init {
        val builder = OkHttpClient.Builder().apply {
            addInterceptor(loggingLevel.provideHttpLoggingInterceptor())
            for (interceptor in interceptorArray) {
                addInterceptor(interceptor)
            }
            connectTimeout(timeout, timeUnit)
        }
        sharedOkHttpClient = builder.build()
    }
}