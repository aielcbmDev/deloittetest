package charly.baquero.deloittetest.retrofit

import io.reactivex.Scheduler
import io.reactivex.Single

class SingleOnSchedulerCallAdapterFactory private constructor(scheduler: Scheduler) :
    RxObserveOnSchedulerCallAdapterFactory<Single<*>>(scheduler) {

    override fun observeOnScheduler(o: Single<*>, scheduler: Scheduler): Single<*> {
        return o.observeOn(scheduler)
    }

    override fun isWrongRawClass(returnClazz: Class<*>): Boolean {
        return returnClazz != CLAZZ
    }

    companion object {
        private val CLAZZ = Single::class.java

        @JvmStatic
        fun create(scheduler: Scheduler): SingleOnSchedulerCallAdapterFactory {
            return SingleOnSchedulerCallAdapterFactory(scheduler)
        }
    }
}