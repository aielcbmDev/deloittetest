package charly.baquero.deloittetest.retrofit

import io.reactivex.Scheduler
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * "Making Retrofit Work For You" by Jake Wharton
 *
 *
 * Please, watch minute 46:48
 * https://www.youtube.com/watch?v=t34AQlblSeE
 */
abstract class RxObserveOnSchedulerCallAdapterFactory<T>(private val scheduler: Scheduler) :
    CallAdapter.Factory() {

    @Suppress("UNCHECKED_CAST")
    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        val returnClazz = getRawType(returnType)
        if (isWrongRawClass(returnClazz)) {
            return null
        }
        val delegate =
            retrofit.nextCallAdapter(this, returnType, annotations) as CallAdapter<Any, T>
        return object : CallAdapter<Any, T> {
            override fun responseType(): Type {
                return delegate.responseType()
            }

            override fun adapt(call: Call<Any>): T {
                val o = delegate.adapt(call) as T
                return observeOnScheduler(o, scheduler)
            }
        }
    }

    protected abstract fun observeOnScheduler(o: T, scheduler: Scheduler): T
    protected abstract fun isWrongRawClass(returnClazz: Class<*>): Boolean

}