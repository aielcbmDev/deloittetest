package charly.baquero.deloittetest.retrofit.okhttp

import okhttp3.logging.HttpLoggingInterceptor

class LoggingLevel(isDebugBuild: Boolean) {

    private val loggingLevel =
        if (isDebugBuild) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = loggingLevel
        return httpLoggingInterceptor
    }
}