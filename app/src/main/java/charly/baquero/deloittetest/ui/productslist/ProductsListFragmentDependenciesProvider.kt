package charly.baquero.deloittetest.ui.productslist

import charly.baquero.deloittetest.di.scopes.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ProductsListFragmentDependenciesProvider {

    @ContributesAndroidInjector(modules = [ProductsListFragmentModule::class])
    @PerFragment
    abstract fun provideProductsFragmentFactory(): ProductsListFragment
}