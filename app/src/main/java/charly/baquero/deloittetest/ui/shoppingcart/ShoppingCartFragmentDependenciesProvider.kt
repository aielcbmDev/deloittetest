package charly.baquero.deloittetest.ui.shoppingcart

import charly.baquero.deloittetest.di.scopes.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ShoppingCartFragmentDependenciesProvider {

    @ContributesAndroidInjector(modules = [ShoppingCartFragmentModule::class])
    @PerFragment
    abstract fun provideShoppingCartFragmentFactory(): ShoppingCartFragment
}