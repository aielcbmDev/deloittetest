package charly.baquero.deloittetest.ui.productslist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import charly.baquero.deloittetest.R
import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import charly.baquero.deloittetest.ui.productslist.views.productlistitem.ProductListItemView

class ProductListAdapter(private val onAddButtonClickListener: OnAddButtonClickListener) :
    RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {

    private var productList: List<Product> = listOf()

    private val onClickListener = View.OnClickListener { v ->
        val product = v.tag as Product
        onAddButtonClickListener.onAddButtonClick(product)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val productListItemView =
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.product_recycler_view_item_layout,
                    parent,
                    false
                ) as ProductListItemView
        val viewHolder = ViewHolder(productListItemView)
        viewHolder.productListItemView.setAddButtonOnClickListener(onClickListener)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = productList[position]
        holder.productListItemView.setData(product)
    }

    override fun getItemCount() = productList.size

    fun setProductList(productList: List<Product>) {
        this.productList = productList
    }

    override fun getItemId(position: Int): Long = productList[position].id

    inner class ViewHolder(val productListItemView: ProductListItemView) :
        RecyclerView.ViewHolder(productListItemView)

    interface OnAddButtonClickListener {

        fun onAddButtonClick(product: Product)
    }
}