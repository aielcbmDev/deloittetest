package charly.baquero.deloittetest.ui.wishlist

import charly.baquero.deloittetest.di.scopes.PerFragment
import dagger.Module
import dagger.Provides

@Module
class WishListFragmentModule {

    @Provides
    @PerFragment
    fun provideWishListFragmentView(wishListFragment: WishListFragment): WishListFragmentContract.View {
        return wishListFragment
    }

    @Provides
    @PerFragment
    fun provideWishListFragmentPresenter(): WishListFragmentContract.Presenter {
        return WishListFragmentPresenter()
    }
}