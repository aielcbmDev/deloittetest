package charly.baquero.deloittetest.ui.productslist

import charly.baquero.deloittetest.repository.ClothesRepository
import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import io.reactivex.observers.DisposableSingleObserver

class ProductsListFragmentPresenter(
    private val productsListFragmentView: ProductsListFragmentContract.View,
    private val clothesRepository: ClothesRepository
) : ProductsListFragmentContract.Presenter {

    override fun requestProductsList() {
        productsListFragmentView.hideErrorView()
        productsListFragmentView.displayProgressBar()
        clothesRepository.getProductsList()
            .subscribe(object : DisposableSingleObserver<List<Product>>() {
                override fun onSuccess(productsList: List<Product>) {
                    onDataRequestSucceeded(productsList)
                }

                override fun onError(error: Throwable) {
                    onDataRequestFailed()
                }
            })
    }

    override fun addProductToCart(productId: Long) {
        productsListFragmentView.hideErrorView()
        productsListFragmentView.displayProgressBar()
        clothesRepository.addProductToCart(productId)
            .subscribe(object : DisposableSingleObserver<List<Product>>() {
                override fun onSuccess(productsList: List<Product>) {
                    onDataRequestSucceeded(productsList)
                }

                override fun onError(error: Throwable) {
                    onDataRequestFailed()
                }
            })
    }

    private fun updateData(productsList: List<Product>) {
        productsListFragmentView.updateListData(productsList)
        productsListFragmentView.notifyListDataChanged()
        productsListFragmentView.hideProgressBar()
    }

    private fun provideNonNullItemsList(productsList: List<Product>?): List<Product> {
        return if (productsList.isNullOrEmpty()) {
            listOf()
        } else {
            productsList
        }
    }

    private fun onDataRequestSucceeded(productsList: List<Product>?) {
        val nonNullItemsList = provideNonNullItemsList(productsList)
        updateData(nonNullItemsList)
        productsListFragmentView.hideErrorView()
    }

    private fun onDataRequestFailed() {
        val nonNullItemsList = provideNonNullItemsList(null)
        updateData(nonNullItemsList)
        productsListFragmentView.displayErrorView()
    }
}