package charly.baquero.deloittetest.ui.shoppingcart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import charly.baquero.deloittetest.R
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ShoppingCartFragment : DaggerFragment(), ShoppingCartFragmentContract.View {

    @Inject
    lateinit var shoppingCartFragmentPresenter: ShoppingCartFragmentContract.Presenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.shopping_cart_list_fragment_layout, container, false)
    }
}
