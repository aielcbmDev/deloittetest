package charly.baquero.deloittetest.ui.productslist.views.productlistitem

import charly.baquero.deloittetest.repository.network.getproducts.response.Product

interface ProductListItemViewContract {

    interface View {

        fun setName(name: String)

        fun setCategory(category: String)

        fun setPrice(price: String)

        fun setOldPrice(oldPrice: String)

        fun setOldPriceVisibility(visibility: Int)

        fun setItemsInStock(itemsInStock: String)

        fun setAddButtonTag(product: Product)

        fun setAddButtonVisibility(visibility: Int)
    }

    interface Presenter {

        fun setProduct(product: Product)
    }
}