package charly.baquero.deloittetest.ui.productslist.views.productlistitem

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import android.widget.TextView
import charly.baquero.deloittetest.R
import charly.baquero.deloittetest.repository.network.getproducts.response.Product

class ProductListItemView : RelativeLayout, ProductListItemViewContract.View {

    private lateinit var nameTextView: TextView
    private lateinit var categoryTextView: TextView
    private lateinit var priceTextView: TextView
    private lateinit var oldPriceTextView: TextView
    private lateinit var stockTextView: TextView
    private lateinit var addButtonTextView: TextView

    private lateinit var productListItemPresenter: ProductListItemViewContract.Presenter

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    override fun onFinishInflate() {
        super.onFinishInflate()
        productListItemPresenter = ProductListItemPresenter(this)
        nameTextView = findViewById(R.id.product_recycler_view_item_name)
        categoryTextView = findViewById(R.id.product_recycler_view_item_category)
        priceTextView = findViewById(R.id.product_recycler_view_item_price)
        oldPriceTextView = findViewById(R.id.product_recycler_view_item_old_price)
        stockTextView = findViewById(R.id.product_recycler_view_item_stock)
        addButtonTextView = findViewById(R.id.product_recycler_view_item_add_to_cart)
    }

    fun setData(product: Product) {
        productListItemPresenter.setProduct(product)
    }

    fun setAddButtonOnClickListener(onClickListener: OnClickListener) {
        addButtonTextView.setOnClickListener(onClickListener)
    }

    override fun setName(name: String) {
        nameTextView.text = name
    }

    override fun setCategory(category: String) {
        categoryTextView.text = context.getString(R.string.product_item_category_text, category)
    }

    override fun setPrice(price: String) {
        priceTextView.text = context.getString(R.string.product_item_price_value_text, price)
    }

    override fun setOldPrice(oldPrice: String) {
        oldPriceTextView.text =
            context.getString(R.string.product_item_old_price_value_text, oldPrice)
    }

    override fun setOldPriceVisibility(visibility: Int) {
        oldPriceTextView.visibility = visibility
    }

    override fun setItemsInStock(itemsInStock: String) {
        stockTextView.text = context.getString(R.string.product_item_stock_text, itemsInStock)
    }

    override fun setAddButtonTag(product: Product) {
        addButtonTextView.tag = product
    }

    override fun setAddButtonVisibility(visibility: Int) {
        addButtonTextView.visibility = visibility
    }
}