package charly.baquero.deloittetest.ui.productslist

import charly.baquero.deloittetest.di.scopes.PerFragment
import charly.baquero.deloittetest.repository.ClothesDataRepository
import charly.baquero.deloittetest.repository.ClothesRepository
import charly.baquero.deloittetest.repository.network.addproducttocart.AddProductToCart
import charly.baquero.deloittetest.repository.network.addproducttocart.AddProductToCartDataSource
import charly.baquero.deloittetest.repository.network.addproducttocart.AddProductToCartRetrySettings
import charly.baquero.deloittetest.repository.network.addproducttocart.AddProductToCartService
import charly.baquero.deloittetest.repository.network.getitemsincart.GetItemsInCart
import charly.baquero.deloittetest.repository.network.getitemsincart.GetItemsInCartDataSource
import charly.baquero.deloittetest.repository.network.getitemsincart.GetItemsInCartRetrySettings
import charly.baquero.deloittetest.repository.network.getitemsincart.GetItemsInCartService
import charly.baquero.deloittetest.repository.network.getproducts.GetProductsList
import charly.baquero.deloittetest.repository.network.getproducts.GetProductsListDataSource
import charly.baquero.deloittetest.repository.network.getproducts.GetProductsListService
import charly.baquero.deloittetest.repository.network.getproducts.GetProductsRetrySettings
import charly.baquero.deloittetest.retrofit.SingleOnSchedulerCallAdapterFactory
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

@Module
class ProductsListFragmentModule {

    @Provides
    @PerFragment
    fun provideProductsListFragmentView(productsListFragment: ProductsListFragment): ProductsListFragmentContract.View {
        return productsListFragment
    }

    @Provides
    @PerFragment
    fun provideGetProductsListService(
        sharedOkHttpClient: OkHttpClient,
        url: String,
        moshi: Moshi
    ): GetProductsListService {
        val builder = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(SingleOnSchedulerCallAdapterFactory.create(AndroidSchedulers.mainThread()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(sharedOkHttpClient)
        val retrofit = builder.baseUrl(url).build()
        return retrofit.create(GetProductsListService::class.java)
    }

    @Provides
    @PerFragment
    fun provideGetProductsListDataSource(
        getProductsListService: GetProductsListService
    ): GetProductsListDataSource {
        val retrySettings = GetProductsRetrySettings(3, 1L, TimeUnit.SECONDS)
        return GetProductsList(getProductsListService, retrySettings)
    }

    @Provides
    @PerFragment
    fun provideAddProductToCartService(
        sharedOkHttpClient: OkHttpClient,
        url: String,
        moshi: Moshi
    ): AddProductToCartService {
        val builder = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(SingleOnSchedulerCallAdapterFactory.create(AndroidSchedulers.mainThread()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(sharedOkHttpClient)
        val retrofit = builder.baseUrl(url).build()
        return retrofit.create(AddProductToCartService::class.java)
    }

    @Provides
    @PerFragment
    fun provideAddProductToCartDataSource(
        addProductToCartService: AddProductToCartService
    ): AddProductToCartDataSource {
        val retrySettings = AddProductToCartRetrySettings(3, 1L, TimeUnit.SECONDS)
        return AddProductToCart(addProductToCartService, retrySettings)
    }

    @Provides
    @PerFragment
    fun provideGetItemsInCartService(
        sharedOkHttpClient: OkHttpClient,
        url: String,
        moshi: Moshi
    ): GetItemsInCartService {
        val builder = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(SingleOnSchedulerCallAdapterFactory.create(AndroidSchedulers.mainThread()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(sharedOkHttpClient)
        val retrofit = builder.baseUrl(url).build()
        return retrofit.create(GetItemsInCartService::class.java)
    }

    @Provides
    @PerFragment
    fun provideGetItemsInCartDataSource(
        getItemsInCartService: GetItemsInCartService
    ): GetItemsInCartDataSource {
        val retrySettings = GetItemsInCartRetrySettings(3, 1L, TimeUnit.SECONDS)
        return GetItemsInCart(getItemsInCartService, retrySettings)
    }

    @Provides
    @PerFragment
    fun provideProductsRepository(
        getProductsListDataSource: GetProductsListDataSource,
        addProductToCartDataSource: AddProductToCartDataSource,
        getItemsInCartDataSource: GetItemsInCartDataSource
    ): ClothesRepository {
        return ClothesDataRepository(
            getProductsListDataSource,
            addProductToCartDataSource,
            getItemsInCartDataSource
        )
    }

    @Provides
    @PerFragment
    fun provideProductsFragmentPresenter(
        productsListFragmentView: ProductsListFragmentContract.View,
        clothesRepository: ClothesRepository
    ): ProductsListFragmentContract.Presenter {
        return ProductsListFragmentPresenter(productsListFragmentView, clothesRepository)
    }
}