package charly.baquero.deloittetest.ui.productslist

import charly.baquero.deloittetest.repository.network.getproducts.response.Product

interface ProductsListFragmentContract {

    interface View {

        fun displayProgressBar()

        fun hideProgressBar()

        fun displayErrorView()

        fun hideErrorView()

        fun updateListData(productsList: List<Product>)

        fun notifyListDataChanged()
    }

    interface Presenter {

        fun requestProductsList()

        fun addProductToCart(productId: Long)
    }
}