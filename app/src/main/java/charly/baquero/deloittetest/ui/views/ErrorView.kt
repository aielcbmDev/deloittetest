package charly.baquero.deloittetest.ui.views

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import android.widget.RelativeLayout
import charly.baquero.deloittetest.R

class ErrorView : RelativeLayout {

    private lateinit var onErrorViewListener: OnErrorViewListener

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    override fun onFinishInflate() {
        super.onFinishInflate()
        val retryButton = findViewById<Button>(R.id.error_view_retry_button)
        retryButton.setOnClickListener { onErrorViewListener.onRetryButtonClick() }
    }

    fun setOnErrorViewListener(onErrorViewListener: OnErrorViewListener) {
        this.onErrorViewListener = onErrorViewListener
    }

    interface OnErrorViewListener {
        fun onRetryButtonClick()
    }
}