package charly.baquero.deloittetest.ui.productslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import charly.baquero.deloittetest.R
import charly.baquero.deloittetest.repository.network.getproducts.response.Product
import charly.baquero.deloittetest.ui.views.ErrorView
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ProductsListFragment : DaggerFragment(), ProductsListFragmentContract.View {

    @Inject
    lateinit var productsListFragmentPresenter: ProductsListFragmentContract.Presenter

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var errorView: ErrorView
    private lateinit var productListAdapter: ProductListAdapter

    private val onClickListener = object : ProductListAdapter.OnAddButtonClickListener {
        override fun onAddButtonClick(product: Product) {
            productsListFragmentPresenter.addProductToCart(product.id)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.products_list_fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.products_list_fragment_layout_progress_bar)
        recyclerView = view.findViewById(R.id.products_list_fragment_layout_recycler_view)
        productListAdapter = ProductListAdapter(onClickListener)
        productListAdapter.setHasStableIds(true)
        recyclerView.adapter = productListAdapter
        errorView = view.findViewById(R.id.products_list_fragment_layout_error_view)
        errorView.setOnErrorViewListener(object : ErrorView.OnErrorViewListener {
            override fun onRetryButtonClick() {
                productsListFragmentPresenter.requestProductsList()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        productsListFragmentPresenter.requestProductsList()
    }

    override fun displayProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun displayErrorView() {
        errorView.visibility = View.VISIBLE
    }

    override fun hideErrorView() {
        errorView.visibility = View.GONE
    }

    override fun updateListData(productsList: List<Product>) {
        productListAdapter.setProductList(productsList)
    }

    override fun notifyListDataChanged() {
        productListAdapter.notifyDataSetChanged()
    }
}
