package charly.baquero.deloittetest.ui.wishlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import charly.baquero.deloittetest.R
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class WishListFragment : DaggerFragment(), WishListFragmentContract.View {

    @Inject
    lateinit var wishListFragmentPresenter: WishListFragmentContract.Presenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.wish_list_fragment_layout, container, false)
    }
}
