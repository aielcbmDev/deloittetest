package charly.baquero.deloittetest.ui

import charly.baquero.deloittetest.di.scopes.PerActivity
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    @PerActivity
    fun provideMainActivityView(mainActivity: MainActivity): MainActivityContract.View {
        return mainActivity
    }

    @Provides
    @PerActivity
    fun provideMainActivityPresenter(): MainActivityContract.Presenter {
        return MainActivityPresenter()
    }

}