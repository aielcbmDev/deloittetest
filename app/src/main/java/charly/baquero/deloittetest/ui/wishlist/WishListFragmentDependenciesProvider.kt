package charly.baquero.deloittetest.ui.wishlist

import charly.baquero.deloittetest.di.scopes.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class WishListFragmentDependenciesProvider {

    @ContributesAndroidInjector(modules = [WishListFragmentModule::class])
    @PerFragment
    abstract fun provideWishListFragmentFactory(): WishListFragment
}