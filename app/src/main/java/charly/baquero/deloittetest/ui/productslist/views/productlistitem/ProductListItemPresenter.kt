package charly.baquero.deloittetest.ui.productslist.views.productlistitem

import android.view.View
import charly.baquero.deloittetest.repository.network.getproducts.response.Product

class ProductListItemPresenter(
    private val productListItemView: ProductListItemViewContract.View
) : ProductListItemViewContract.Presenter {

    override fun setProduct(product: Product) {
        productListItemView.setName(product.name)
        productListItemView.setCategory(product.category)
        productListItemView.setPrice(product.price)
        if (product.isDiscounted()) {
            productListItemView.setOldPriceVisibility(View.VISIBLE)
            productListItemView.setOldPrice(
                product.oldPrice
                    ?: throw IllegalArgumentException("oldPrice CAN NOT be null if the product is discounted")
            )
        } else {
            productListItemView.setOldPriceVisibility(View.INVISIBLE)
        }

        productListItemView.setItemsInStock(product.stock.toString())

        if (product.isInStock()) {
            productListItemView.setAddButtonVisibility(View.VISIBLE)
            productListItemView.setAddButtonTag(product)
        } else {
            productListItemView.setAddButtonVisibility(View.INVISIBLE)
        }
    }
}