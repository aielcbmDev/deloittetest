package charly.baquero.deloittetest.ui.shoppingcart

import charly.baquero.deloittetest.di.scopes.PerFragment
import dagger.Module
import dagger.Provides

@Module
class ShoppingCartFragmentModule {

    @Provides
    @PerFragment
    fun provideShoppingCartFragmentView(shoppingCartFragment: ShoppingCartFragment): ShoppingCartFragmentContract.View {
        return shoppingCartFragment
    }

    @Provides
    @PerFragment
    fun provideShoppingCartFragmentPresenter(): ShoppingCartFragmentContract.Presenter {
        return ShoppingCartFragmentPresenter()
    }
}