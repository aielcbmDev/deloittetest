package charly.baquero.deloittetest.utils

import io.reactivex.Flowable
import io.reactivex.functions.Function
import org.reactivestreams.Publisher
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

class RetryFunction(
    private val numberOfRetries: Int,
    private val timeBetweenRetries: Long,
    private val timeUnit: TimeUnit
) : Function<Flowable<Throwable>, Publisher<out Long>> {

    override fun apply(throwableFlowable: Flowable<Throwable>): Publisher<out Long> {
        val counter = AtomicInteger()
        return throwableFlowable
            .takeWhile { counter.getAndIncrement() < numberOfRetries }
            .flatMap { Flowable.timer(timeBetweenRetries, timeUnit) }
    }
}