package charly.baquero.deloittetest.utils

import java.util.concurrent.TimeUnit

interface RetrySettings {

    fun getNumberOfRetries(): Int

    fun getTimeBetweenRetries(): Long

    fun getRetriesTimeUnit(): TimeUnit
}